package com.angrytest.autotest.resetpwd;

import com.angrytest.pagefactory.annotations.Find;
import com.angrytest.pagefactory.interfaces.ILink;

/**
 * 登陆页面
 */
public class LoginPage {

    /**
     * 忘记密码链接
     */
    @Find("text=>Forgot your password?")
    public ILink forgetPwd;

}
