package com.angrytest.autotest.resetpwd;

import com.angrytest.annotations.DataSource;
import com.angrytest.assertion.Asserter;
import com.angrytest.browsers.Browser;
import com.angrytest.browsers.ChromeBrowser;
import com.angrytest.pagefactory.PO;
import org.testng.annotations.Test;

import java.util.Map;

public class TestResetPwd {

    @Test
    @DataSource(file = "account.xls")
    public void resetPwd(Map<String, String> param) {
        Browser browser = ChromeBrowser.start("https://gitlab.com/users/sign_in");
        LoginPage login = new LoginPage();
        ResetPwdPage resetPwd = new ResetPwdPage();
        PO.initPage(login, browser);
        PO.initPage(resetPwd, browser);
        login.forgetPwd.click();
        resetPwd.email.sendKeys(param.get("email")+"@qq.com");
        resetPwd.commit.click();
        Asserter asserter = new Asserter(browser);
        asserter.assertEquals("If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes.",resetPwd.msg.text());
        Asserter.assertAll();
    }
}
