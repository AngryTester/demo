package com.angrytest.autotest.resetpwd;

import com.angrytest.pagefactory.annotations.Find;
import com.angrytest.pagefactory.interfaces.IDiv;
import com.angrytest.pagefactory.interfaces.IInput;

/**
 * 重置密码页面
 */
public class ResetPwdPage {

    /**
     * 邮箱输入框
     */
    @Find("id=>user_email")
    public IInput email;

    /**
     * 提交按钮
     */
    @Find("name=>commit")
    public IInput commit;

    /**
     * 提示信息
     */
    @Find("class=>container-fluid container-limited ")
    public IDiv msg;

}
